# Selenium de 0 a 80...
_En una semana logré realizar un script para testing automatizado de un formulario de aplicación web sin conocimientos previos de Selenium y con básicos conocimientos de Python. Te cuento cómo._

[Un artículo para linkedin](https://www.linkedin.com/pulse/selenium-de-0-80-jos%25C3%25A9-luis-balle)

En este repo encontras:

```[files] Los archivos html que simulan la app```

```[screenshot] Las capturas de pantalla```

```test.py El primer script, sin funciones```

```test01.py El segundo script, con funciones```

Reitero que es un primer acercamiento a ambas herramientas.